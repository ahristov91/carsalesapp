export const USERNAME_MIN_LENGTH = 3
export const USERNAME_MAX_LENGTH = 30
export const USER_FIRST_NAME_MIN_LENGTH = 1
export const USER_FIRST_NAME_MAX_LENGTH = 30
export const USER_LAST_NAME_MIN_LENGTH = 1
export const USER_LAST_NAME_MAX_LENGTH = 30
export const PHONE_NUMBER_LENGTH = 10

export const CONTEST_TITLE_MIN_LENGTH = 1
export const CONTEST_TITLE_MAX_LENGTH = 60
export const PHOTO_TITLE_MIN_LENGTH = 1
export const PHOTO_TITLE_MAX_LENGTH = 30

export const PHOTO_STORY_MIN_LENGTH = 10
export const PHOTO_STORY_MAX_LENGTH = 120

export const PASSWORD_MIN_LENGTH = 6
export const PASSWORD_MAX_LENGTH = 16

export const EMAIL_MIN_LENGTH = 6
export const EMAIL_MAX_LENGTH = 100
