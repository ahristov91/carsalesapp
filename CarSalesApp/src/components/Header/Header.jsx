import React, { useState, useRef, useContext } from 'react'
import { Link, useNavigate } from 'react-router-dom'
import { Transition } from '@headlessui/react'
import AppContext from '../../context/app.context'



export default function Header() {
  const [isOpen, setIsOpen] = useState(false)
  const navigate = useNavigate()
  const ref = useRef()
  const { addToast, setAppState, userData, ...appState } = useContext(AppContext)

  return (
    <div>
      <nav className="bg-base-100 shadow-md">
        <div className="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8">
          <div className="flex items-center justify-between h-16">
            <div className="flex items-center">
              <div className="text-2xl font-bold ">
                <a>TheCarSalesApp</a>
              </div>
              <div className="hidden md:block">
                <div className="ml-10 flex items-baseline space-x-4">
                  <Link
                    to={'/'}
                    // href="#"
                    className=" hover:bg-amber-100 px-3 py-2 rounded-md text-md font-medium">
                    Home
                  </Link>

                  <a
                    // href="#"
                    className=" hover:bg-amber-100 px-3 py-2 rounded-md text-md font-medium">
                    <Link to={'/Offers'}>Offers</Link>
                  </a>

                  <a
                    // href="#"
                    className=" hover:bg-amber-100 px-3 py-2 rounded-md text-md font-medium">
                    <Link to={'/offers/createoffer'}>Create Offer</Link>
                  </a>
                </div>
              </div>
            </div>
            <>
              {appState.user === null && (
                <ul className="menu menu-horizontal">
                  <li tabIndex={0}>
                    <span className=" hover:bg-amber-100 font-bold rounded-md">
                      Login / Register
                    </span>
                    <ul className=" hover:bg-amber-50 font-bold rounded-lg">
                      <li>
                        <Link to={'/login'}>Login</Link>
                      </li>
                      <li>
                        <Link to={'/register'}>Register</Link>
                      </li>
                    </ul>
                  </li>
                </ul>
              )}
              {appState.user !== null && (
                <div>
                  <div className="dropdown dropdown-end text-md text-primary font-bold">
                    <a>
                      {' '}
                      Username {''}{' '}
                      <label tabIndex={0} className="btn btn-ghost btn-circle avatar">
                        <div className="w-10 rounded-full">
                          <img src="https://placeimg.com/80/80/people" />
                        </div>
                      </label>
                    </a>
                    <ul
                      tabIndex={0}
                      className="menu menu-compact dropdown-content mt-3 p-2 shadow bg-amber-100 rounded-box w-52">
                      <li>
                        <Link to={`/profile`} className="justify-between">
                          {/* <Link to={`/profile${userId}`} className="justify-between"> */}
                          Profile
                        </Link>
                      </li>
                      <li>
                        <a>
                          Notifications{' '}
                          <button className="btn btn-ghost btn-circle">
                            <div className="indicator">
                              <svg
                                xmlns="http://www.w3.org/2000/svg"
                                className="h-5 w-5"
                                fill="none"
                                viewBox="0 0 24 24"
                                stroke="currentColor">
                                <path
                                  strokeLinecap="round"
                                  strokeLinejoin="round"
                                  strokeWidth="2"
                                  d="M15 17h5l-1.405-1.405A2.032 2.032 0 0118 14.158V11a6.002 6.002 0 00-4-5.659V5a2 2 0 10-4 0v.341C7.67 6.165 6 8.388 6 11v3.159c0 .538-.214 1.055-.595 1.436L4 17h5m6 0v1a3 3 0 11-6 0v-1m6 0H9"
                                />
                              </svg>
                              <span className="badge badge-xs badge-primary indicator-item"></span>
                            </div>
                          </button>
                        </a>
                      </li>
                      <li>
                        <a>Logout</a>
                      </li>
                    </ul>
                  </div>
                </div>
              )}
            </>
            <div className="-mr-2 flex md:hidden">
              <button
                onClick={() => setIsOpen(!isOpen)}
                type="button"
                className="bg-amber-100 inline-flex items-center justify-center p-2 rounded-md text-secondary hover:bg-amber-100  focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-offset-amber-300  focus:ring-amber-50"
                aria-controls="mobile-menu"
                aria-expanded="false">
                <span className="sr-only">Open main menu</span>
                {!isOpen ? (
                  <svg
                    className="block h-6 w-6"
                    xmlns="http://www.w3.org/2000/svg"
                    fill="none"
                    viewBox="0 0 24 24"
                    stroke="currentColor"
                    aria-hidden="true">
                    <path
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      strokeWidth="2"
                      d="M4 6h16M4 12h16M4 18h16"
                    />
                  </svg>
                ) : (
                  <svg
                    className="block h-6 w-6"
                    xmlns="http://www.w3.org/2000/svg"
                    fill="none"
                    viewBox="0 0 24 24"
                    stroke="currentColor"
                    aria-hidden="true">
                    <path
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      strokeWidth="2"
                      d="M6 18L18 6M6 6l12 12"
                    />
                  </svg>
                )}
              </button>
            </div>
          </div>
        </div>

        <Transition
          show={isOpen}
          enter="transition ease-out duration-100 transform"
          enterFrom="opacity-0 scale-95"
          enterTo="opacity-100 scale-100"
          leave="transition ease-in duration-75 transform"
          leaveFrom="opacity-100 scale-100"
          leaveTo="opacity-0 scale-95">
          {
            <div className="md:hidden" id="mobile-menu">
              <div ref={ref} className="px-2 pt-2 pb-3 space-y-1 sm:px-3">
                <a
                  //   href="#"
                  className="hover:bg-amber-50  text-secondary block px-3 py-2 rounded-md text-base font-medium">
                  <Link to={'/'}>Home</Link>
                </a>

                <a
                  //   href="#"
                  className="hover:bg-amber-50 text-secondary block px-3 py-2 rounded-md text-base font-medium">
                  <Link to={'/contests'}>Contests</Link>
                </a>

                <a
                  //   href="#"
                  className="hover:bg-amber-50  text-secondary block px-3 py-2 rounded-md text-base font-medium">
                  <Link to={'/contests/createcontest'}>Create Contest</Link>
                </a>
              </div>
            </div>
          }
        </Transition>
      </nav>

      <main>
        <div className="max-w-7xl mx-auto py-6 sm:px-6 lg:px-8"></div>
      </main>
    </div>
  )
}
