import { useState } from 'react'

export const OfferMenu = () => {
  
  const [userRole, setUserRole] = useState(2)

  return (
    <div className="w-full p-10 ">
      {userRole === 1 && (
        <div>
          <div className="flex justify-evenly gap-10 flex-wrap whitespace-pre-wrap ">
            <div className="card w-[300px] bg-base-100 shadow-xl image-full">
              <figure>
                <img src="https://placeimg.com/400/225/arch" alt="Shoes" />
              </figure>
              <div className="card-body">
                <h2 className="card-title">Активни обеви</h2>
                <p>В момента има 18 обяви приемащи залог</p>
                <p>22 регистрирани 132 качени снимки</p>
                <div className="card-actions justify-end">
                  <button className="btn btn-primary">Клик</button>
                </div>
              </div>
            </div>

            <div className="card w-[300px] bg-base-100 shadow-xl image-full">
              <figure>
                <img src="https://placeimg.com/400/225/arch" alt="Shoes" />
              </figure>
              <div className="card-body">
                <h2 className="card-title">За ревю</h2>
                <p>12 обяви във 2-ра фаза</p>
                <div className="card-actions justify-end">
                  <button className="btn btn-primary">Цък</button>
                </div>
              </div>
            </div>

            <div className="card w-[300px] bg-base-100  shadow-xl image-full">
              <figure>
                <img src="https://placeimg.com/400/225/arch" alt="Shoes" />
              </figure>
              <div className="card-body">
                <h2 className="card-title">Приключили</h2>
                <p>Разгледай победителите</p>
                <div className="card-actions justify-end">
                  <button className="btn btn-primary">ЩРАК</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      )}

      {userRole === 2 && (
        <div>
          <div className="flex justify-evenly gap-10 flex-wrap whitespace-pre-wrap ">
            <div className="card w-[300px] bg-base-100  shadow-xl image-full">
              <figure>
                <img src="https://placeimg.com/400/225/arch" alt="Shoes" />
              </figure>
              <div className="card-body">
                <h2 className="card-title">Запиши се сега</h2>
                <p>В момента има 18 обяви приемащи залог</p>
                <div className="card-actions justify-end">
                  <button className="btn">Клик</button>
                </div>
              </div>
            </div>

            <div className="card w-[300px] bg-base-100 shadow-xl image-full">
              <figure>
                <img src="https://placeimg.com/400/225/arch" alt="Shoes" />
              </figure>
              <div className="card-body">
                <h2 className="card-title">Неприключили участия</h2>
                <p>Записан за 12 обяви</p>
                <p>3 обяви без снимка</p>
                <div className="card-actions justify-end">
                  <button className="btn">Цък</button>
                </div>
              </div>
            </div>

            <div className="card w-[300px] bg-base-100  shadow-xl image-full">
              <figure>
                <img src="https://placeimg.com/400/225/arch" alt="Shoes" />
              </figure>
              <div className="card-body">
                <h2 className="card-title">Изтекли обяви</h2>
                <p>Разгледай рейтинга си</p>
                <div className="card-actions justify-end">
                  <button className="btn">ЩРАК</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      )}
      <div className="flex justify-center p-10 gap-5">
        <button onClick={() => setUserRole(1)} className="btn flex-1">
          {/* change Role -> 1 */}
        </button>
        <button onClick={() => setUserRole(2)} className="btn flex-[2]">
          {/* change Role -> 2 */}
        </button>
      </div>
    </div>
  )
}
