
import { initializeApp } from "firebase/app";
import { getAuth } from 'firebase/auth';
import { getDatabase } from 'firebase/database';
import { getStorage } from 'firebase/storage'


const firebaseConfig = {
  apiKey: "AIzaSyBIrHCWpZ2ggw9gOt7fpqJE6TCFY_4AzRE",
  authDomain: "carsalesapp-6dfc9.firebaseapp.com",
  projectId: "carsalesapp-6dfc9",
  storageBucket: "carsalesapp-6dfc9.appspot.com",
  messagingSenderId: "820326075966",
  appId: "1:820326075966:web:c40ab75fcfc20bf64ecb6f",
  databaseURL: "https://carsalesapp-6dfc9-default-rtdb.europe-west1.firebasedatabase.app/"
};


export const app = initializeApp(firebaseConfig);
export const auth = getAuth(app);
export const db = getDatabase(app);
export const storage = getStorage(app)
