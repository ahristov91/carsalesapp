export function makeSymmetricalEnum(object) {
    return Object.keys(object).map((key) => {
      Object.defineProperty(object, object[key], {
        value: key,
        enumerable: false,
      })
    })
  }
  