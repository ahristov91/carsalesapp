export const points = {
    joinOpenContest: 1,
    invitedToContest: 3,
    thirdPlace: 20,
    thirdPlaceShared: 10,
    secondPlace: 35,
    secondPlaceShared: 25,
    firstPlace: 50,
    firstPlaceShared: 40,
  }
  