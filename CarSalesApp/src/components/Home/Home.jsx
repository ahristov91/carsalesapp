export const Home = () => {

    return (
      <div className="hero min-h-screen" style={{ backgroundImage: `url("https://hips.hearstapps.com/hmg-prod/images/2023-mclaren-artura-101-1655218102.jpg?crop=1.00xw:0.847xh;0,0.153xh&resize=1200:*")` }}>
    <div className="hero-overlay bg-opacity-60"></div>
    <div className="hero-content text-center text-neutral-content">
      <div className="max-w-md">
        <h1 className="mb-5 text-5xl font-bold">The Car Enthusiasts Portal</h1>
        <p className="mb-5">Fuel Your Passion. Drive Your Dreams.</p>
        
      </div>
    </div>
  </div> 
    )
  }