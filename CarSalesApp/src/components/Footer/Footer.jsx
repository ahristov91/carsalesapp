export default function Footer() {
    return (
      <footer className="footer footer-center p-4 bg-base-100 shadow-inner text-base-content fixed bottom-0">
        <div>
          <p className="font-">Copyright © 2023 - All right reserved by TheCarSalesApp Team</p>
        </div>
      </footer>
    )
  }
  