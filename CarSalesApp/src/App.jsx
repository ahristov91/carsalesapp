// import { BrowserRouter } from 'react-router-dom'
import './App.css'
import Header from './components/Header/Header'
// import Login from './views/Login/Login'
import { Route, Routes } from 'react-router'
import { BrowserRouter } from 'react-router-dom'
import { useAuthState } from 'react-firebase-hooks/auth'
import { useState } from 'react'
import { auth } from './firebase/config'
import Footer from './components/Footer/Footer'
import { Home } from './components/Home/Home'
import { Offers } from './views/Offers/Offers'

useAuthState
function App() {
  const [user, loading, error] = useAuthState(auth)

  const [appState, setAppState] = useState({
    theme: localStorage.getItem('theme') || 'light',
    user: user ? { email: user.email, uid: user.uid } : null,
    userData: null,
  })

  return (
    <BrowserRouter>
      <div className="App">
        <Header />
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/offers" element={<Offers />} />
          {/* <Route path="/login" element={<Login />} /> */}
          {/* <Route path='/contests' element={<Contests />} /> */}
        </Routes>
        <Footer />
      </div>
    </BrowserRouter>
  )
}

export default App

